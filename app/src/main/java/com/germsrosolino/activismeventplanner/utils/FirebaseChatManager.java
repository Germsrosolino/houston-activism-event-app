package com.germsrosolino.activismeventplanner.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.germsrosolino.activismeventplanner.interfaces.FirebaseCallBacks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class FirebaseChatManager implements ChildEventListener{
    private volatile static FirebaseChatManager firebaseChatManager;
    private DatabaseReference messageReference;
    private FirebaseCallBacks callBacks;

    public static synchronized FirebaseChatManager getInstance(String roomName,
                                                               FirebaseCallBacks callBacks) {
        if(firebaseChatManager == null) {
            synchronized (FirebaseChatManager.class) {
                firebaseChatManager = new FirebaseChatManager(roomName, callBacks);
            }
        }
        return firebaseChatManager;
    }

    private FirebaseChatManager(String roomName, FirebaseCallBacks callBacks) {
        messageReference = FirebaseDatabase.getInstance().getReference().child(roomName);
        this.callBacks = callBacks;
    }

    public void addMessageListener() { messageReference.addChildEventListener(this); }

    public void removeListener() { messageReference.removeEventListener(this);}

    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        callBacks.onNewMessage(dataSnapshot);
    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    public void sendMessageToFirebase(String message) {
        Map<String, Object> map = new HashMap<>();
        map.put("text", message);
        map.put("time", System.currentTimeMillis());
        map.put("senderId", FirebaseAuth.getInstance().getCurrentUser().getUid());

        String keyToPush = messageReference.push().getKey();
        messageReference.child(keyToPush).setValue(map);
    }

    public void destroy() {
        firebaseChatManager=null;
        callBacks=null;
    }
}
