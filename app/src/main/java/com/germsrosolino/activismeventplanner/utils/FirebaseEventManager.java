package com.germsrosolino.activismeventplanner.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.germsrosolino.activismeventplanner.interfaces.FirebaseInterface;
import com.germsrosolino.activismeventplanner.models.AllUsers;
import com.germsrosolino.activismeventplanner.models.Caller;
import com.germsrosolino.activismeventplanner.models.EventList;
import com.germsrosolino.activismeventplanner.models.Friends;
import com.germsrosolino.activismeventplanner.models.MyAttending;
import com.germsrosolino.activismeventplanner.models.User;
import com.germsrosolino.activismeventplanner.models.UsersWhoRequested;
import com.germsrosolino.activismeventplanner.views.main.MainActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import com.germsrosolino.activismeventplanner.models.Event;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class FirebaseEventManager implements FirebaseInterface {

    private static final String TAG = "FirebaseEventManager";
    ImageView ivLogo;

    @Override
    public void getAllEvents(final Context context) {
        DatabaseReference eventReference = FirebaseDatabase.getInstance().getReference(Constant.EVENTS);

        eventReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                final Event event = dataSnapshot.getValue(Event.class);

                if(event != null) {
                    event.setId(dataSnapshot.getKey());

                    Map<String, String> attending = new HashMap<>();
                    for (DataSnapshot snap : dataSnapshot.child(Constant.IDS_OF_ATTENDING).getChildren()) {
                        if (snap.getValue() != null) {
                            attending.put(snap.getValue().toString(), snap.getValue().toString());
                        }
                    }
                    event.setIdsOfAttending(attending);
                    setupEvent(event, Constant.ADD_NEW_EVENT, context);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                final Event event = dataSnapshot.getValue(Event.class);
                if (event != null) {
                    event.setId(dataSnapshot.getKey());
                    Map<String, String> attending = new HashMap<>();
                    for (DataSnapshot snap : dataSnapshot.child(Constant.IDS_OF_ATTENDING).getChildren()) {
                        if (snap.getValue() != null) {
                            attending.put(snap.getValue().toString(), snap.getValue().toString());
                        }
                    }
                    event.setIdsOfAttending(attending);
                    setupEvent(event, Constant.UPDATE_EVENT, context);
                }
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                final Event event = dataSnapshot.getValue(Event.class);
                if (event != null) {
                    event.setId(dataSnapshot.getKey());
                    setupEvent(event, Constant.DELETE_EVENT, context);
                }
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setupEvent(final Event event, final String task, final Context context) {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageReference = storage.getReferenceFromUrl("gs://activism-planner.appspot.com");

        // TODO: 6/22/2018 set distance functionality
        storageReference.child("images/" + event.getId() + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                event.setImageURL(uri.toString());
                //Log.d(TAG, "onSuccess: " +event.getImageURL());
                //event.setDistance(getDistanceFromDeviceLocation(event, context));
                EventList eventList = EventList.getInstance(context);

                switch (task) {
                    case Constant.ADD_NEW_EVENT:
                        eventList.getEvents().add(event);
                        break;
                    case Constant.UPDATE_EVENT:
                        eventList.getEvents().set(eventList.getEvents().indexOf(event), event);
                        break;
                    case Constant.DELETE_EVENT:
                        eventList.getEvents().remove(eventList.getEvents().indexOf(event));
                        break;
                }
                EventBus.getDefault().post(new Caller(""));
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                EventList eventList = EventList.getInstance(context);

                switch (task) {
                    case Constant.ADD_NEW_EVENT:
                        eventList.getEvents().add(event);
                        break;
                    case Constant.UPDATE_EVENT:
                        eventList.getEvents().set(eventList.getEvents().indexOf(event), event);
                        break;
                    case Constant.DELETE_EVENT:
                        eventList.getEvents().remove(eventList.getEvents().indexOf(event));
                        break;
                }
                EventBus.getDefault().post(new Caller(""));
            }
        });
    }

    @Override
    public void saveAttending(String userId, String eventId, String hostId) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();

            final DatabaseReference eventReference = database.getReference(Constant.EVENTS);
            eventReference
                    .child(eventId)
                    .child(Constant.IDS_OF_ATTENDING)
                    .child(userId)
                    .setValue(userId);

            final DatabaseReference profileReference = database.getReference(Constant.PROFILES);
            profileReference
                    .child(hostId)
                    .child(Constant.IDS_OF_ATTENDING)
                    .child(userId)
                    .setValue(userId);

            profileReference
                    .child(userId)
                    .child(Constant.IDS_OF_USER_ATTENDING)
                    .child(eventId)
                    .setValue(eventId);
        }
    }

    @Override
    public void sendFriendRequest(String currentUserId, String friendUserId) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();

            final DatabaseReference profileReference = database.getReference(Constant.PROFILES);
            profileReference
                    .child(friendUserId)
                    .child(Constant.FRIEND_REQUEST_LIST)
                    .child(currentUserId)
                    .setValue(currentUserId);

            final DatabaseReference profileReferenceTwo = database.getReference(Constant.PROFILES);
            profileReferenceTwo
                    .child(currentUserId)
                    .child(Constant.CURRENT_USER_REQUESTED)
                    .child(friendUserId)
                    .setValue(friendUserId);
        }
    }

    @Override
    public void acceptFriendRequest(String currentUserId, String friendUserId) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();

            final DatabaseReference profileReference = database.getReference(Constant.PROFILES);
            profileReference
                    .child(currentUserId)
                    .child(Constant.FRIENDS)
                    .child(friendUserId)
                    .setValue(friendUserId);

            profileReference
                    .child(friendUserId)
                    .child(Constant.FRIENDS)
                    .child(currentUserId)
                    .setValue(currentUserId);

            final DatabaseReference profileReferenceTwo = database.getReference(Constant.PROFILES);
            profileReferenceTwo
                    .child(currentUserId)
                    .child(Constant.FRIEND_REQUEST_LIST)
                    .child(friendUserId)
                    .setValue(null);

            profileReferenceTwo
                    .child(friendUserId)
                    .child(Constant.CURRENT_USER_REQUESTED)
                    .child(currentUserId)
                    .setValue(null);
        }
    }

    @Override
    public void getMyFriends() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference profileReference = database.getReference(Constant.PROFILES).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Constant.FRIENDS);

        profileReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> userIdsOfFriends = new ArrayList<>();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    userIdsOfFriends.add(snapshot.getKey());
                }
                EventBus.getDefault().post(new Friends(userIdsOfFriends));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void removeAttending(String userId, String eventId, String hostId) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {

            FirebaseDatabase database = FirebaseDatabase.getInstance();

            final DatabaseReference eventReference = database.getReference(Constant.EVENTS);
        }
    }

    @Override
    public void removeFriend(String userId, String friendUserId) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();

            final DatabaseReference profileReference = database.getReference(Constant.PROFILES);
            profileReference
                    .child(userId)
                    .child(Constant.FRIENDS)
                    .child(friendUserId)
                    .setValue(null);

            profileReference
                    .child(friendUserId)
                    .child(Constant.FRIENDS)
                    .child(userId)
                    .setValue(null);
        }
    }

    @Override
    public void getAttending() {
        final List<String> attending = new ArrayList<>();

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference profileReference = database.getReference(Constant.PROFILES);

        profileReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot
                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Constant.IDS_OF_USER_ATTENDING).getChildren()) {
                    attending.add(snapshot.getValue().toString());
                }
                EventBus.getDefault().post(new MyAttending(attending));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void getPendingFriendRequests() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference profileReference = database.getReference(Constant.PROFILES)
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Constant.FRIEND_REQUEST_LIST);

        profileReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> userIdsRequested = new ArrayList<>();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    userIdsRequested.add(snapshot.getKey());
                }
                EventBus.getDefault().post(new UsersWhoRequested(userIdsRequested));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void addEvent(final Event event, final Bitmap bitmap) {
        UUID id = UUID.randomUUID();
        final String idString = id.toString();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        if(bitmap !=null) {
            final FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageReference = storage.getReference();
            final StorageReference logoReference = storageReference.child(idString);
            final StorageReference logoImagesReference = storageReference.child("images/" + idString + ".jpg");


            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            final byte[] data = baos.toByteArray();

            final UploadTask uploadTask = logoReference.putBytes(data);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d(TAG, "onFailure: ");
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    logoReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            event.setImageURL(uri.toString());
                            Log.d(TAG, "onSuccess: " + uri);
                            Log.d(TAG, "onSuccess: " + event.getImageURL());
                            Log.d(TAG, "addEvent: " + event.getImageURL());
                            final DatabaseReference profileReference = database.getReference(Constant.PROFILES);
                            profileReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Constant.EVENTS).child(idString).setValue(event);

                            DatabaseReference eventReference = database.getReference(Constant.EVENTS);
                            eventReference.child(idString).setValue(event);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d(TAG, "onFailure: ");
                        }
                    });

                }
            });
        }

    }



    @Override
    public void editEvent(final Event event, Bitmap bitmap) {
        if(bitmap != null) {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageReference = storage.getReferenceFromUrl("gs://activism-planner.appspot.com");
            StorageReference mountainImageRef = storageReference.child("images/" + event.getId() + ".jpg");
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] data = byteArrayOutputStream.toByteArray();
            UploadTask uploadTask = mountainImageRef.putBytes(data);

            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    saveEditEvent(event);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    saveEditEvent(event);
                }
            });
        } else {
            saveEditEvent(event);
        }
    }

    private void saveEditEvent(Event event) {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        final DatabaseReference profileReference = database.getReference(Constant.PROFILES);

        profileReference
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Constant.EVENTS)
                .child(event.getId())
                .child(Constant.HOSTID)
                .setValue(event.getHostId());

        profileReference
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Constant.EVENTS)
                .child(event.getId())
                .child(Constant.EVENT_NAME)
                .setValue(event.getHostId());

        profileReference
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Constant.EVENTS)
                .child(event.getId())
                .child(Constant.DESCRIPTION)
                .setValue(event.getHostId());

        profileReference
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Constant.EVENTS)
                .child(event.getId())
                .child(Constant.LOCATION)
                .setValue(event.getHostId());

        profileReference
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Constant.EVENTS)
                .child(event.getId())
                .child(Constant.DATE)
                .setValue(event.getHostId());

        profileReference
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Constant.EVENTS)
                .child(event.getId())
                .child(Constant.START_TIME)
                .setValue(event.getHostId());

        profileReference
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Constant.EVENTS)
                .child(event.getId())
                .child(Constant.END_TIME)
                .setValue(event.getHostId());

        profileReference
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Constant.EVENTS)
                .child(event.getId())
                .child(Constant.IMAGE_URL)
                .setValue(event.getHostId());

        profileReference
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Constant.EVENTS)
                .child(event.getId())
                .child(Constant.HOSTID)
                .setValue(event.getHostId());


        DatabaseReference eventReference = database.getReference(Constant.EVENTS);

        eventReference
                .child(event.getId())
                .child(Constant.HOSTID)
                .setValue(event.getHostId());

        eventReference
                .child(event.getId())
                .child(Constant.EVENT_NAME)
                .setValue(event.getHostId());

        eventReference
                .child(event.getId())
                .child(Constant.DESCRIPTION)
                .setValue(event.getHostId());

        eventReference
                .child(event.getId())
                .child(Constant.LOCATION)
                .setValue(event.getHostId());

        eventReference
                .child(event.getId())
                .child(Constant.DATE)
                .setValue(event.getHostId());

        eventReference
                .child(event.getId())
                .child(Constant.START_TIME)
                .setValue(event.getHostId());

        eventReference
                .child(event.getId())
                .child(Constant.END_TIME)
                .setValue(event.getHostId());

        eventReference
                .child(event.getId())
                .child(Constant.IMAGE_URL)
                .setValue(event.getHostId());


    }

    public void getAllUsers() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference profileReference = database.getReference(Constant.PROFILES);

        profileReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> userIds = new ArrayList<>();
                List<String> usernameList = new ArrayList<>();
                List<String> listOfAlreadyFriends = new ArrayList<>();
                List<String> listOfAlreadyRequested = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    userIds.add(snapshot.getKey());
                    usernameList.add(snapshot.child(Constant.USERNAME).getValue().toString());

                    if(FirebaseAuth.getInstance().getCurrentUser().getUid().equals(snapshot.getKey())) {
                        for(DataSnapshot snap : snapshot.child(Constant.FRIENDS).getChildren()) {
                            listOfAlreadyFriends.add(snap.getKey());
                        }
                        for(DataSnapshot snap : snapshot.child(Constant.CURRENT_USER_REQUESTED).getChildren()) {
                            listOfAlreadyRequested.add(snap.getKey());
                        }
                    }
                }
                EventBus.getDefault().post(new AllUsers(userIds, usernameList, listOfAlreadyFriends, listOfAlreadyRequested));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void updateImageURL(String url) {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference profileReference = database.getReference(Constant.PROFILES);

        profileReference
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child(Constant.IMAGE_URL)
                .setValue(url);
    }

    @Override
    public void getMyUserName() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference profileReference = database.getReference(Constant.PROFILES).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Constant.USERNAME);

        profileReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                EventBus.getDefault().post(new User(dataSnapshot.getValue().toString()));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void updateUserName(final String userName, final String oldUserName) {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference firebaseReference = database.getReference();
        firebaseReference.child(Constant.USERNAMES).child(userName).runTransaction(new Transaction.Handler() {
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                if (mutableData.getValue() == null) {
                    mutableData.setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
                    return Transaction.success(mutableData);
                }
                return Transaction.abort();
            }

            @Override
            public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {
                if(b) {
                    final DatabaseReference profileReference = database.getReference(Constant.PROFILES).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Constant.USERNAME);
                    profileReference.setValue(userName);

                    final DatabaseReference usernameReference = database.getReference(Constant.USERNAMES).child(oldUserName);
                    usernameReference.setValue(null);
                    EventBus.getDefault().post(new Caller("Username Updated"));
                } else {
                    EventBus.getDefault().post(new Caller("Username already taken, please try something else"));
                }
            }
        });
    }
}
