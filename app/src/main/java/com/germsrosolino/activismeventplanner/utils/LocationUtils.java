//package com.germsrosolino.activismeventplanner.utils;
//
//import android.Manifest;
//import android.content.Context;
//import android.content.pm.PackageManager;
//import android.location.Address;
//import android.location.Criteria;
//import android.location.Location;
//import android.location.LocationManager;
//import android.provider.ContactsContract;
//import android.support.v4.app.ActivityCompat;
//
//import com.germsrosolino.activismeventplanner.models.Event;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//
//import java.io.IOException;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//import java.util.Locale;
//
//import okhttp3.HttpUrl;
//
//public class LocationUtils {
//    static int apiFailureDistanceFailSafe = 1;
//
//    public static CustomLocationObject setGeographicalLocation(Location location) {
//        CustomLocationObject returnCustomLocation = new CustomLocationObject();
//        returnCustomLocation.setLatitude(location.getLatitude());
//        returnCustomLocation.setLongitude(location.getLongitude());
//        returnCustomLocation.setLatitude_longitude(new LatLng(location.getLatitude(), location.getLongitude()));
//        return returnCustomLocation;
//    }
//    static String locationProvider;
//    public static final CustomLocationObject getPhysicalDeviceLocation(Context context) {
//        LocationManager locationManager;
//
//        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
//        locationProvider = locationManager.getBestProvider(new Criteria(), false);
//        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED&& ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            return null;
//        }
//        Location physicalDeviceLocation = locationManager.getLastKnownLocation(locationProvider);
//        return setGeographicalLocation(physicalDeviceLocation);
//    }
//
//    public static final String getDistanceFromDeviceLocation(Event passedEvent, Context context) throws IOException {
//        CustomLocationObject currentDeviceLocObject = getPhysicalDeviceLocation(context);
//        Address passedEventAddress = parseStringToAddress(passedEvent.getAddress());
//        CustomLocationObject passedEventLocation = getLatitudeLongitudeOfAddress(new CustomLocationObject(), passedEventAddress, locationProvider);
//        return getDistanceAsTheCrowFlies(currentDeviceLocObject, passedEventLocation);
//    }
//
//    public static List<Event> setEventDistance(List<Event> passEventList, Context context) throws IOException {
//        int error = 0;
//        for(Event beingEvalEvent : passEventList) {
//            if (beingEvalEvent.getAddress() != null) {
//                beingEvalEvent.setDistance(getDistanceFromDeviceLocation(beingEvalEvent, context));
//            } else {
//                beingEvalEvent.setDistance("100,000.00" + error);
//                error = error + 2;
//            }
//        }
//        Collections.sort(passEventList, new Comparator<Event>() {
//            @Override
//            public int compare(Event o1, Event o2) {
//                if(o1.getDistance() != null && o2.getDistance() != null) {
//                    Double d1 = Double.parseDouble(o1.getDistance().replace(",", "").replaceAll("[^\\d.]", ""));
//                    Double d2 = Double.parseDouble(o2.getDistance().replace(",", "").replaceAll("[^\\d.]", ""));
//                }
//                return 0;
//            }
//        });
//        return  passEventList;
//    }
//
//    private static void saveEventEdit(Event event) {
//        FirebaseDatabase database = FirebaseDatabase.getInstance();
//        final DatabaseReference profileReference = database.getReference(Constant.PROFILES);
//        profileReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(Constant.EVENTS).child(event.getId()).setValue(event);
//
//        DatabaseReference eventReference = database.getReference(Constant.EVENTS);
//        eventReference.child(event.getId()).setValue(event);
//    }
//
//    public static String getDistanceAsTheCrowFlies(CustomLocationObject currentLocation, CustomLocationObject requestedLocation) {
//        if(requestedLocation.getLatitude() != null) {
//            double lat1 = currentLocation.getLatitude();
//            double lat2 = requestedLocation.getLatitude();
//            double lng1 = currentLocation.getLongitude();
//            double lng2 = requestedLocation.getLongitude();
//            double earthRadius = 6371000;
//            double dLat = Math.toRadians(lat2 - lat1);
//            double dLng = Math.toRadians(lng2 - lng1);
//            double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
//                    Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
//                            Math.sin(dLng / 2) * Math.sin(dLng / 2);
//            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
//            float dist = (float) (earthRadius * c);
//
//            return String.format("%.2f", ConversionUtilities.convertMetersToMiles(dist));
//        } else {
//            return String.valueOf(apiFailureDistanceFailSafe++);
//        }
//    }
//
//    private static GeocodingProfile returnedGeoProfile;
//    public static CustomLocationObject getLatitudeLongitudeOfAddress(CustomLocationObject passedLocationObject, Address passedAddress, String provider) throws IOException {
//        String address = passedAddress.getAddressLine(1) + " " + passedAddress.getLocality() + " " + passedAddress.getPostalCode();
//        final HttpUrl url = new HttpUrl.Builder()
//                .scheme("https")
//                .host("maps.googleapis.com")
//                .addPathSegment("maps")
//                .addPathSegment("api")
//                .addPathSegment("geocode")
//                .addPathSegment("json")
//                .addQueryParameter("address", address)
//                .addQueryParameter("key", GOOGLE_GEO_API_KEY)
//    }
//
//    public static Address parseStringToAddress(String passedString){
//        Address address = new Address(Locale.US);
//        address.setAddressLine(1,passedString);
//        address.setLocality("");
//        address.setPostalCode("");
//        return address;
//
//    }
//}
