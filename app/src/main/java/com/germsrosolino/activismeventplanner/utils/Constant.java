package com.germsrosolino.activismeventplanner.utils;

public class Constant {
    public static final String EVENTS = "events";
    public static final String EVENT_ID = "eventId";
    public static final String PROFILES = "profiles";
    public static final String USERNAME = "username";
    public static final String USERNAMES = "usernames";
    public static final String FRIENDS = "friends";
    public static final String FRIEND_REQUEST_LIST = "friendRequestList";
    public static final String CURRENT_USER_REQUESTED = "currentUserRequested";
    public static final String IDS_OF_ATTENDING = "idsOfAttending";
    public static final String IDS_OF_USER_ATTENDING = "idsOfUserAttending";
    public static final String ID = "id";
    public static final String HOSTID = "hostId";
    public static final String EVENT_NAME = "eventName";
    public static final String DESCRIPTION = "description";
    public static final String LOCATION = "location";
    public static final String DATE = "date";
    public static final String START_TIME = "startTime";
    public static final String END_TIME = "endTime";
    public static final String IMAGE_URL = "imageURL";
    public static final String ADD_NEW_EVENT = "add";
    public static final String UPDATE_EVENT = "update";
    public static final String DELETE_EVENT = "delete";
    public static final String DISTANCE = "distance";
    public static final String LATLNG = "latlng";
    public static final String TEXT = "text";
    public static final String TIME = "time";
    public static final String SENDER_ID = "senderId";
    public static String EXTRA_ROOM_NAME = "extra room name";

    public static final double RADIUS_OF_EARTH = 637;
    public static final double METERS_TO_MILES_RATIO = 0.000621371;
    public static final double MILES_TO_METERS_RATIO = 1609.344;
}
