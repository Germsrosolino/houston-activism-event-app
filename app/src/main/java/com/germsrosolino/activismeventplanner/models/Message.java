package com.germsrosolino.activismeventplanner.models;

import com.germsrosolino.activismeventplanner.interfaces.ModelCallBacks;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;

public class Message {
    private ArrayList<Chat> messages;

    public void addMessages(DataSnapshot dataSnapshot, ModelCallBacks callBacks) {
        if (messages==null) {
            messages= new ArrayList<>();
        }
        Chat chat=new Chat(dataSnapshot);
        messages.add(chat);
        callBacks.onModelUpdated(messages);
    }
}
