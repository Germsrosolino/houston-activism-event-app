package com.germsrosolino.activismeventplanner.models;

import java.util.List;

public class MyAttending {
    private List<String> attending;

    public MyAttending(List<String> attending) {
        this.attending = attending;
    }

    public List<String> getAttending() { return attending; }
}
