package com.germsrosolino.activismeventplanner.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class Event implements Parcelable{
    private String id;
    private String hostId;
    private String eventName;
    private String description;
    private String address;
    private String location;
    private String date;
    private String endDate;
    private String startTime;
    private String endTime;
    private String imageURL;
    private String distance;
    private String latlng;

    private Map<String, String> idsOfAttending;

    public Event() {idsOfAttending = new HashMap<>();}

    @Override
    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        } else if(!(obj instanceof Event)) {
            return false;
        }
        Event p = (Event) (obj);
        return this.getId().equals(p.getId());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getLatlng() {
        return latlng;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    public Map<String, String> getIdsOfAttending() {
        return idsOfAttending;
    }

    public void setIdsOfAttending(Map<String, String> idsOfAttending) {
        this.idsOfAttending = idsOfAttending;
    }

    public String getAddress() { return address; }

    public static Creator<Event> getCreator() {return CREATOR; }

    protected Event(Parcel in) {
        id = in.readString();
        hostId = in.readString();
        eventName = in.readString();
        description = in.readString();
        location = in.readString();
        date = in.readString();
        endDate = in.readString();
        startTime = in.readString();
        endTime = in.readString();
        imageURL = in.readString();
        idsOfAttending = new HashMap<>();
        distance = in.readString();
        latlng = in.readString();
        in.readMap(idsOfAttending, String.class.getClassLoader());
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) { return new Event(in); }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeString(hostId);
        parcel.writeString(eventName);
        parcel.writeString(description);
        parcel.writeString(location);
        parcel.writeString(date);
        parcel.writeString(endDate);
        parcel.writeString(startTime);
        parcel.writeString(endTime);
        parcel.writeString(imageURL);
        parcel.writeMap(idsOfAttending);
    }
}
