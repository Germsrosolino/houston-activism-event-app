package com.germsrosolino.activismeventplanner.models;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public class EventList {

    private Context context;
    private static EventList eventList;
    private static List<Event> events = new ArrayList<>();

    private EventList(Context context) { this.context = context; }

    public static EventList getInstance(Context context) {
        if (eventList == null) {
            return new EventList(context);
        } else {
            return eventList;
        }
    }

    public List<Event> getEvents() {
        return events;
    }

    public static void setEvents(List<Event> events) {
        EventList.events = events;
    }
}
