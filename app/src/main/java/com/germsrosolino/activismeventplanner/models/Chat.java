package com.germsrosolino.activismeventplanner.models;

import com.germsrosolino.activismeventplanner.utils.Constant;
import com.google.firebase.database.DataSnapshot;

import java.util.HashMap;

public class Chat {
    private String msgKey;
    private long timeStamp;
    private String message;
    private String senderId;

    public Chat(DataSnapshot dataSnapshot) {
        HashMap<String, Object> object = (HashMap<String, Object>) dataSnapshot.getValue();
        this.msgKey=dataSnapshot.getKey();
        this.message=object.get(Constant.TEXT).toString();
        this.senderId=object.get(Constant.SENDER_ID).toString();
        this.timeStamp=Long.parseLong(object.get(Constant.TIME).toString());
    }

    public String getMsgKey() {
        return msgKey;
    }

    public void setMsgKey(String msgKey) {
        this.msgKey = msgKey;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }
}
