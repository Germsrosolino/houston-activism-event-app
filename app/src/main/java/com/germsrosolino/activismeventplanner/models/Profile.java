package com.germsrosolino.activismeventplanner.models;

import java.util.ArrayList;
import java.util.List;

public class Profile {
    private String id;
    private String email;
    private String username;
    private List<Event> events;
    private List<String> attendingParties;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public List<String> getAttendingParties() {
        return attendingParties;
    }

    public void setAttendingParties(List<String> attendingParties) {
        this.attendingParties = attendingParties;
    }




}
