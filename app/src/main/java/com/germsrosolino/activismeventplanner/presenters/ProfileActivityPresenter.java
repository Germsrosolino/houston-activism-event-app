package com.germsrosolino.activismeventplanner.presenters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.germsrosolino.activismeventplanner.views.profile.ProfileActivityContract;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ProfileActivityPresenter implements ProfileActivityContract.presenter {
    private static final String TAG = "ProfileActivityPresente";

    private FirebaseUser user;
    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private Context context;
    private ProfileActivityContract.view view;

    @Override
    public void init(Context context, FirebaseAuth auth, FirebaseAuth.AuthStateListener authStateListener) {
        this.context = context;
        this.auth = auth;
        this.authStateListener = authStateListener;
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
        auth.addAuthStateListener(authStateListener);
    }

    @Override
    public void removeAuthStateListener() {
        if (authStateListener != null) {
            auth.removeAuthStateListener(authStateListener);
        }
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public void attachView(ProfileActivityContract.view view) { this.view = view; }

    @Override
    public void removeView() { this.view = null; }
}
