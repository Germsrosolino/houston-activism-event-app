package com.germsrosolino.activismeventplanner.presenters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.germsrosolino.activismeventplanner.utils.FirebaseEventManager;
import com.germsrosolino.activismeventplanner.views.BaseView;
import com.germsrosolino.activismeventplanner.views.main.MainActivityContract;

public class MainActivityPresenter implements MainActivityContract.Presenter {
    private MainActivityContract.MainView view;
    private Context context;
    private FirebaseEventManager firebaseEventManager = new FirebaseEventManager();


    @Override
    public void attachView(MainActivityContract.MainView view) {
        this.view = view;
    }

    @Override
    public void removeView() {
        this.view = null;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public void getEventsFromFirebase() { firebaseEventManager.getAllEvents(context); }
}
