package com.germsrosolino.activismeventplanner.presenters;

import com.germsrosolino.activismeventplanner.interfaces.FirebaseCallBacks;
import com.germsrosolino.activismeventplanner.interfaces.ModelCallBacks;
import com.germsrosolino.activismeventplanner.models.Chat;
import com.germsrosolino.activismeventplanner.models.Message;
import com.germsrosolino.activismeventplanner.utils.FirebaseChatManager;
import com.germsrosolino.activismeventplanner.views.chat.ChatActivityContract;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;

public class ChatPresenter implements FirebaseCallBacks, ModelCallBacks {
    private ChatActivityContract chatActivityContract;
    private Message messageModel;

    public ChatPresenter(ChatActivityContract chatActivityContract) {
        this.chatActivityContract = chatActivityContract;
        this.messageModel = new Message();
    }

    public void sendMessageToFirebase(String roomName, String message) {
        if(!message.trim().equals("")){
            FirebaseChatManager.getInstance(roomName, this).sendMessageToFirebase(message);
        }
        chatActivityContract.clearEditText();
    }

    public void setListener(String roomName) {
        FirebaseChatManager.getInstance(roomName, this).addMessageListener();
    }

    public void onDestroy(String roomName) {
        FirebaseChatManager.getInstance(roomName, this).removeListener();
        FirebaseChatManager.getInstance(roomName, this).destroy();
        chatActivityContract =null;
    }

    @Override
    public void onNewMessage(DataSnapshot dataSnapshot) { messageModel.addMessages(dataSnapshot, this); }

    @Override
    public void onModelUpdated(ArrayList<Chat> messages) {
        if (messages.size()>0) {
            chatActivityContract.updateList(messages);
        }
    }
}
