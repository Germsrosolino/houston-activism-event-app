package com.germsrosolino.activismeventplanner.presenters;


import com.germsrosolino.activismeventplanner.views.BaseView;
import com.germsrosolino.activismeventplanner.views.createroom.CreateRoomActivityContract;

public class CreateRoomActivityPresenter implements CreateRoomActivityContract.CreateRoomPresenter {
    private CreateRoomActivityContract.CreateRoomView createRoomView;

    public CreateRoomActivityPresenter(CreateRoomActivityContract.CreateRoomView createRoomView) {
        this.createRoomView = createRoomView;
    }

    @Override
    public void invalidateRoom(String roomName) {
        if(roomName.trim().isEmpty()) {
            createRoomView.showToast("Please enter a valid room name");
        } else {
            createRoomView.startChatActivity(roomName);
        }
    }

    @Override
    public void showRoomDialogInActivity() {
        createRoomView.showRoomDialog();
    }

    @Override
    public void attachView(BaseView view) {

    }

    @Override
    public void removeView() {

    }
}
