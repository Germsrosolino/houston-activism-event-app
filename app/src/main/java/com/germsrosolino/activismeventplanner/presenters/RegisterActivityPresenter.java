package com.germsrosolino.activismeventplanner.presenters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.germsrosolino.activismeventplanner.views.BaseView;
import com.germsrosolino.activismeventplanner.views.login.LoginActivityContract;
import com.germsrosolino.activismeventplanner.views.main.MainActivity;
import com.germsrosolino.activismeventplanner.views.register.RegisterActivity;
import com.germsrosolino.activismeventplanner.views.register.RegisterActivityContract;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class RegisterActivityPresenter implements RegisterActivityContract.RegisterPresenter{
    private static final String TAG = "RegisterActivityPresent";

    private LoginActivityContract.LoginView view;
    private Context context;
    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseUser user;



    @Override
    public void init(Context context, FirebaseAuth auth, FirebaseAuth.AuthStateListener authStateListener) {
        this.auth = auth;
        this.authStateListener = authStateListener;
        this.context = context;
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
        auth.addAuthStateListener(authStateListener);
    }

    @Override
    public void removeAuthStateListener() {
        if (authStateListener != null) {
            auth.removeAuthStateListener(authStateListener);
        }
    }


    @Override
    public void attachView(LoginActivityContract.LoginView view) {
        this.view = view;
    }

    @Override
    public void removeView() {
        this.view = null;
        if (authStateListener != null) {
            auth.removeAuthStateListener(authStateListener);
        }
    }

    public void initializeFirebase() {
        auth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
            }
        };
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void signIn(String email, String password) {
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener((RegisterActivity)context, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            view.signInResult(true);
                            Intent homeIntent = new Intent(context, MainActivity.class);
                            context.startActivity(homeIntent);

                        }
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            view.signInResult(false);
                        }

                    }
                });
    }


}
