package com.germsrosolino.activismeventplanner.presenters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.germsrosolino.activismeventplanner.R;
import com.germsrosolino.activismeventplanner.models.Profile;
import com.germsrosolino.activismeventplanner.views.BaseView;
import com.germsrosolino.activismeventplanner.views.chat.ChatActivity;
import com.germsrosolino.activismeventplanner.views.login.LoginActivityContract;
import com.germsrosolino.activismeventplanner.views.login.LoginActivity;
import com.germsrosolino.activismeventplanner.views.main.MainActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.germsrosolino.activismeventplanner.views.login.LoginActivityContract.*;

public class LoginPresenter implements LoginActivityContract.LoginPresenter, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    private static final String TAG = "TAG: ";
    private LoginActivityContract iLoginView;
    private LoginView view;
    private Context context;

    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseUser user;
    private GoogleApiClient googleApiClient;
    private FirebaseAuth firebaseAuth;

    @Override
    public void attachView(LoginActivityContract.LoginView view) {this.view = view; }

    public void removeView() {
        this.view = null;
        if (authStateListener != null) {
            auth.removeAuthStateListener(authStateListener);
        }
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void initializeFirebase() {
        auth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
//                if (user != null) {
//                    //user is signed in
//                } else {
//                    //user is signed out
//                }
            }
        };
    }

    @Override
    public void signIn(String email, String password) {
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener((LoginActivity)context, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            view.signInResult(true);
                            Intent homeIntent = new Intent(context, MainActivity.class);
                            context.startActivity(homeIntent);
                        }
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            view.signInResult(false);
                        }

                    }
                });
    }

    @Override
    public void removeAuthStateListener() {
        if (authStateListener != null) {
            auth.removeAuthStateListener(authStateListener);
        }
    }

    @Override
    public void init(Context context, FirebaseAuth auth, FirebaseAuth.AuthStateListener authStateListener) {
        this.auth = auth;
        this.authStateListener = authStateListener;
        this.context = context;
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };
        auth.addAuthStateListener(authStateListener);
    }

    private void goToSecondActivity(LoginActivity loginActivity) {
        Intent intent = new Intent(loginActivity, MainActivity.class);
        loginActivity.startActivity(intent);
    }

    @Override
    public void loginWithGoogleSetUp(LoginActivity loginActivity, FirebaseAuth firebaseAuth) {
        GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(loginActivity.getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(context)
                .enableAutoManage(loginActivity, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, options)
                .build();

        view.googleApiClientReady(googleApiClient);
        this.firebaseAuth = firebaseAuth;
    }

    @Override
    public void firebaseAuthWithGoogle(GoogleSignInAccount acct, final LoginActivity loginActivity) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(loginActivity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            Log.d(TAG, "onComplete: success");
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            goToSecondActivity(loginActivity);

                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference profileReference = database.getReference("profiles");

                            Profile profile = new Profile();
                            profile.setEmail(user.getEmail());
                            profileReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(profile);
                        } else {
                            Toast.makeText(context, "Authentication Failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
