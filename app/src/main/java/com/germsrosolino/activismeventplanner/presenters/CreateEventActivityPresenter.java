package com.germsrosolino.activismeventplanner.presenters;

import android.graphics.Bitmap;

import com.germsrosolino.activismeventplanner.models.Event;
import com.germsrosolino.activismeventplanner.utils.FirebaseEventManager;
import com.germsrosolino.activismeventplanner.views.createevent.CreateEventActivityContract;

public class CreateEventActivityPresenter implements CreateEventActivityContract.presenter{

    private static final String TAG = "CreateEventActivityPres";
    private CreateEventActivityContract.view view;

    @Override
    public void createNewEvent(Event event, Bitmap bitmap) {
        FirebaseEventManager firebaseEventManager = new FirebaseEventManager();
        firebaseEventManager.addEvent(event, bitmap);
        view.eventSaved(true);
    }

    @Override
    public void attachView(CreateEventActivityContract.view view) { this.view = view; }

    @Override
    public void removeView() { this.view = null; }
}
