package com.germsrosolino.activismeventplanner.views.main;

import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.germsrosolino.activismeventplanner.R;
import com.germsrosolino.activismeventplanner.models.Caller;
import com.germsrosolino.activismeventplanner.models.Event;
import com.germsrosolino.activismeventplanner.models.EventList;
import com.germsrosolino.activismeventplanner.presenters.MainActivityPresenter;
import com.germsrosolino.activismeventplanner.utils.DepthPageTransformer;
import com.germsrosolino.activismeventplanner.utils.FirebaseEventManager;
import com.germsrosolino.activismeventplanner.views.createevent.CreateEventActivity;
import com.germsrosolino.activismeventplanner.views.createroom.CreateRoomActivity;
import com.germsrosolino.activismeventplanner.views.fragment.EventFragment;
import com.germsrosolino.activismeventplanner.views.login.LoginActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import static com.google.firebase.auth.FirebaseAuth.getInstance;

public class MainActivity extends AppCompatActivity implements MainActivityContract.MainView, LocationListener {

    private static final String TAG = "MainActivity";

    private MainActivityPresenter mainActivityPresenter;

    ViewPager viewPager;
    private FragmentStatePagerAdapter adapter;
    private ArrayList<Event> events;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Home");
        mainActivityPresenter = new MainActivityPresenter();
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        mainActivityPresenter.attachView(this);
        mainActivityPresenter.setContext(this);
        mainActivityPresenter.getEventsFromFirebase();
        createViewPagerAdapter();
        viewPager.setAdapter(adapter);
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        switch (item.getItemId()) {
            case R.id.menu_create_event:
                if (user != null) {
                    startActivity(new Intent(this, CreateEventActivity.class));
                } else {
                    startActivity(new Intent(this, LoginActivity.class));
                }
        }
        return super.onOptionsItemSelected(item);
    }

    public void openMessenger(View view) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, CreateRoomActivity.class);
            startActivity(intent);
        }
    }

    // TODO: 7/7/2018 Link profile activity to button

    private void createViewPagerAdapter() {
        adapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                Event event = EventList.getInstance(getApplicationContext()).getEvents().get(position);
                return EventFragment.newInstance(event.getId());
            }

            @Override
            public int getCount() {
                return EventList.getInstance(getApplicationContext()).getEvents().size();
            }
        };
        Log.i(TAG, "createViewPagerAdapter: " + EventList.getInstance(getApplicationContext()).getEvents().size());
        viewPager.setPageTransformer(true, new DepthPageTransformer());
        viewPager.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(Caller caller) {
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        mainActivityPresenter.removeView();
        super.onDestroy();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
