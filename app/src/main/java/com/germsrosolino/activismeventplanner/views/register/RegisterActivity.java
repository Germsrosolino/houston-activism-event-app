package com.germsrosolino.activismeventplanner.views.register;

import android.content.Context;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.germsrosolino.activismeventplanner.R;
import com.germsrosolino.activismeventplanner.models.Profile;
import com.germsrosolino.activismeventplanner.presenters.RegisterActivityPresenter;
import com.germsrosolino.activismeventplanner.views.login.LoginActivityContract;
import com.germsrosolino.activismeventplanner.views.main.MainActivity;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.util.CrashUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

import javax.security.auth.login.LoginException;

public class RegisterActivity extends AppCompatActivity implements LoginActivityContract.LoginView {
    private static final String TAG = "RegisterActivity";
    private RegisterActivityPresenter presenter;

    private EditText inputEmail;
    private TextInputLayout inputPassword;
    private TextInputLayout confirmPassword;
    private EditText inputUserName;
    private Button btnSignUp;

    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseUser user;
    private GoogleApiClient googleApiClient;
    private boolean userCreated = false;
    private boolean registrationComplete = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        inputEmail = findViewById(R.id.signup_input_email);
        inputPassword = findViewById(R.id.signup_input_layout_password);
        confirmPassword = findViewById(R.id.signup_input_layout_passwordConfirm);
        inputUserName = findViewById(R.id.signup_input_username);

        auth = FirebaseAuth.getInstance();
        presenter = new RegisterActivityPresenter();
        presenter.attachView(this);
        presenter.setContext(this);
        presenter.init(this, auth, authStateListener);
        presenter.initializeFirebase();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(authStateListener);
    }



    @Override
    protected void onStop() {
        super.onStop();
        if (authStateListener != null)
            auth.removeAuthStateListener(authStateListener);
        presenter.removeView();
        presenter.removeAuthStateListener();
    }

    @Override
    public void signInResult(boolean response) {
        if(response) {
            Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void googleApiClientReady(GoogleApiClient googleApiClient) {
        this.googleApiClient = googleApiClient;
    }


//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.btn_signup:
//
//                final String email = inputEmail.getText().toString();
//                final String password = inputPassword.toString();
//                final String username = inputUserName.getText().toString();
//
//                if (!confirmPassword.toString().equals(inputPassword.toString())) {
//                    confirmPassword.setError("Passwords do not match!");
//                    Toast.makeText(this, "Passwords do not match", Toast.LENGTH_SHORT).show();
//                } else if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
//                    inputEmail.setError("Please enter a valid email address");
//                } else {
//
//                    if(!userCreated) {
//                        auth.createUserWithEmailAndPassword(email, password)
//                                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
//                                    @Override
//                                    public void onComplete(@NonNull Task<AuthResult> task) {
//
//                                        if (task.isSuccessful()) {
//                                            userCreated = true;
//                                            createUserProfileDatabase(username, email);
//                                        }else{
//                                            Toast.makeText(RegisterActivity.this, "Email exists", Toast.LENGTH_SHORT).show();
//                                        }
//                                    }
//                                });
//                    }else{
//                        createUserProfileDatabase(username, email);
//                    }
//                    break;
//                }
//        }
//    }

    private void createUserProfileDatabase(final String username, final String email) {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference firebaseRef = database.getReference();
        firebaseRef.child("username").child(username).runTransaction(new Transaction.Handler() {
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                if (mutableData.getValue() == null) {
                    mutableData.setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
                    return Transaction.success(mutableData);
                }

                return Transaction.abort();
            }

            @Override
            public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {
                if (b) {
                    DatabaseReference profileReference = database.getReference("profiles");

                    Profile profile = new Profile();
                    profile.setEmail(email);
                    profile.setUsername(username);
                    profileReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(profile);
                    Toast.makeText(RegisterActivity.this, "Your account was created successfully", Toast.LENGTH_SHORT).show();
                    registrationComplete = true;
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    getApplicationContext().startActivity(intent);
                } else {
                    Toast.makeText(RegisterActivity.this, "Username already exists", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void registerUser(View view) {
        final String email = inputEmail.getText().toString();
        final String password = inputPassword.getEditText().getText().toString();
        final String username = inputUserName.getText().toString();
        final String confirm = confirmPassword.getEditText().getText().toString();
        Log.d(TAG, "registerUser: " + password + " " + confirm);


        if (!(confirm.equals(password))) {
            confirmPassword.setError("Passwords do not match!");
            Toast.makeText(this, "Passwords do not match", Toast.LENGTH_SHORT).show();
        } else if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            inputEmail.setError("Please enter a valid email address");
        } else {

            if(!userCreated) {
                auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                if (task.isSuccessful()) {
                                    userCreated = true;
                                    createUserProfileDatabase(username, email);
                                    onBackPressed();
                                }else{
                                    Toast.makeText(RegisterActivity.this, "Email exists", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }else{
                createUserProfileDatabase(username, email);
            }
        }
    }
}
