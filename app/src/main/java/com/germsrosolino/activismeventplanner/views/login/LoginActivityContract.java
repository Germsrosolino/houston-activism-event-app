package com.germsrosolino.activismeventplanner.views.login;

import android.content.Context;
import android.view.View;

import com.germsrosolino.activismeventplanner.views.BasePresenter;
import com.germsrosolino.activismeventplanner.views.BaseView;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;

public interface LoginActivityContract {

    interface LoginView extends BaseView{
        void signInResult(boolean response);
        void googleApiClientReady(GoogleApiClient googleApiClient);
    }

    interface LoginPresenter extends BasePresenter<LoginView>{
        void signIn(String email, String password);
        void removeAuthStateListener();
        void init(Context context, FirebaseAuth mAuth, FirebaseAuth.AuthStateListener authStateListener);
        void loginWithGoogleSetUp(LoginActivity loginActivity, FirebaseAuth firebaseAuth);
        void firebaseAuthWithGoogle(GoogleSignInAccount acct, LoginActivity loginActivity);
    }
}