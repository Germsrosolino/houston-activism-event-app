package com.germsrosolino.activismeventplanner.views.createroom;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.germsrosolino.activismeventplanner.R;
import com.germsrosolino.activismeventplanner.presenters.CreateRoomActivityPresenter;
import com.germsrosolino.activismeventplanner.utils.Constant;
import com.germsrosolino.activismeventplanner.views.chat.ChatActivity;

public class CreateRoomActivity extends AppCompatActivity implements CreateRoomActivityContract.CreateRoomView{

    private CreateRoomActivityContract.CreateRoomPresenter createRoomPresenter;
    private Dialog chatRoomDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_room_layout);

        createRoomPresenter = new CreateRoomActivityPresenter(this);
    }

    public void createNewRoom(View view) {
        createRoomPresenter.showRoomDialogInActivity();
    }

    public void joinExistingRoom(View view) {
        createRoomPresenter.showRoomDialogInActivity();
    }

    @Override
    public void showRoomDialog() {
        chatRoomDialog = new Dialog(this);
        chatRoomDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_room, null);
        Button submitRoomName = (Button) view.findViewById(R.id.button_room_submit);
        final EditText editTextRoomName = (EditText) view.findViewById(R.id.edittext_room_name);
        submitRoomName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createRoomPresenter.invalidateRoom(editTextRoomName.getText().toString());
            }
        });

        chatRoomDialog.setContentView(view);
        chatRoomDialog.show();
    }

    @Override
    public void startChatActivity(String roomName) {
        chatRoomDialog.dismiss();
        chatRoomDialog = null;

        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(Constant.EXTRA_ROOM_NAME,roomName);
        startActivity(intent);
    }

    @Override
    public void showToast(String message) {

    }
}
