package com.germsrosolino.activismeventplanner.views.chat;

import com.germsrosolino.activismeventplanner.models.Chat;

import java.util.ArrayList;

public interface ChatActivityContract {
    void updateList(ArrayList<Chat> list);
    void clearEditText();
}
