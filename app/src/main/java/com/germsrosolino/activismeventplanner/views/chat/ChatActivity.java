package com.germsrosolino.activismeventplanner.views.chat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import com.germsrosolino.activismeventplanner.R;
import com.germsrosolino.activismeventplanner.models.Chat;
import com.germsrosolino.activismeventplanner.presenters.ChatPresenter;
import com.germsrosolino.activismeventplanner.utils.Constant;

import java.util.ArrayList;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener, ChatActivityContract {
    private EditText editText;
    private ChatPresenter chatPresenter;
    private RecyclerView recyclerView;
    private String roomName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        roomName=getIntent().getStringExtra(Constant.EXTRA_ROOM_NAME);
        chatPresenter =new ChatPresenter(this);
        chatPresenter.setListener(roomName);

        editText=(EditText) findViewById(R.id.edittext_chat_message);
        recyclerView=(RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        findViewById(R.id.button_send_message).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_send_message:
                chatPresenter.sendMessageToFirebase(roomName,editText.getText().toString());
                break;
        }
    }

    @Override
    public void updateList(ArrayList<Chat> list) {
        ChatAdapter chatAdapter=new ChatAdapter(this, list);
        recyclerView.setAdapter(chatAdapter);
        recyclerView.scrollToPosition(list.size()-1);
    }

    @Override
    public void clearEditText() { editText.setText(""); }

    @Override
    public void onDestroy() {
        super.onDestroy();
        chatPresenter.onDestroy(roomName);
    }
}
