package com.germsrosolino.activismeventplanner.views.profile;

import android.content.Context;

import com.germsrosolino.activismeventplanner.views.BasePresenter;
import com.germsrosolino.activismeventplanner.views.BaseView;
import com.google.firebase.auth.FirebaseAuth;

public interface ProfileActivityContract {
    public interface view extends BaseView {

    }

    public interface presenter extends BasePresenter<view> {
        void init(Context context, FirebaseAuth auth, FirebaseAuth.AuthStateListener authStateListener);
        void removeAuthStateListener();
    }
}
