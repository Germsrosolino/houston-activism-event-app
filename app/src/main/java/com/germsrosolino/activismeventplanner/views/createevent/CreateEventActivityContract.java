package com.germsrosolino.activismeventplanner.views.createevent;

import android.graphics.Bitmap;

import com.germsrosolino.activismeventplanner.models.Event;
import com.germsrosolino.activismeventplanner.views.BasePresenter;
import com.germsrosolino.activismeventplanner.views.BaseView;

public interface CreateEventActivityContract {
    interface view extends BaseView {
        void eventSaved(Boolean saved);
    }

    interface presenter extends BasePresenter<CreateEventActivityContract.view> {
        void createNewEvent(Event event, Bitmap bitmap);
    }
}
