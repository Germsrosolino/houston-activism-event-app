package com.germsrosolino.activismeventplanner.views.createroom;


import com.germsrosolino.activismeventplanner.views.BasePresenter;
import com.germsrosolino.activismeventplanner.views.BaseView;

public interface CreateRoomActivityContract {
    interface CreateRoomView extends BaseView {
        void showRoomDialog();
        void startChatActivity(String roomName);
        void showToast(String message);

    }

    interface CreateRoomPresenter extends BasePresenter {
        void invalidateRoom(String roomName);
        void showRoomDialogInActivity();


    }
}
