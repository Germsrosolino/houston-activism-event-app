package com.germsrosolino.activismeventplanner.views.register;

import android.content.Context;
import android.view.View;

import com.germsrosolino.activismeventplanner.views.BasePresenter;
import com.germsrosolino.activismeventplanner.views.BaseView;
import com.germsrosolino.activismeventplanner.views.login.LoginActivityContract;
import com.google.firebase.auth.FirebaseAuth;

public interface RegisterActivityContract {
    interface RegisterView extends BaseView{
        void signInResult(boolean response);
    }

    interface RegisterPresenter extends BasePresenter<LoginActivityContract.LoginView>{
        void signIn(String email, String password);
        void init(Context context, FirebaseAuth auth, FirebaseAuth.AuthStateListener authStateListener);
        void removeAuthStateListener();
    }
}
