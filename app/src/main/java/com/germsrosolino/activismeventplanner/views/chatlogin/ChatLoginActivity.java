package com.germsrosolino.activismeventplanner.views.chatlogin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.germsrosolino.activismeventplanner.R;

public class ChatLoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_login);
    }
}
