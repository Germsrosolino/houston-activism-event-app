package com.germsrosolino.activismeventplanner.views.login;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.germsrosolino.activismeventplanner.R;
import com.germsrosolino.activismeventplanner.presenters.LoginPresenter;
import com.germsrosolino.activismeventplanner.views.chat.ChatActivity;
import com.germsrosolino.activismeventplanner.views.main.MainActivity;
import com.germsrosolino.activismeventplanner.views.register.RegisterActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity implements LoginActivityContract.LoginView {
    private static final String TAG = "LoginActivity";
    private static final int RC_SIGN_IN = 1;
    private AutoCompleteTextView etUserName;
    private EditText etPassword;
    private SignInButton loginWithGoogle;
    private Button btnRegister;

    private LoginPresenter presenter;
    private FirebaseAuth.AuthStateListener authStateListener;
    private GoogleApiClient googleApiClient;
    private FirebaseAuth auth;
    private FirebaseUser user;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUserName = findViewById(R.id.etUserName);
        etPassword = findViewById(R.id.etPassword);
        loginWithGoogle = findViewById(R.id.login_with_google);
        btnRegister = findViewById(R.id.btnRegister);

        auth = FirebaseAuth.getInstance();
        presenter = new LoginPresenter();
        presenter.attachView(this);
        presenter.setContext(this);
        presenter.initializeFirebase();
        presenter.init(this, auth, authStateListener);
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {

                }
            }
        };
        loginWithGoogle.setSize(SignInButton.SIZE_ICON_ONLY);
        loginWithGoogle.setColorScheme(SignInButton.COLOR_LIGHT);
        presenter.loginWithGoogleSetUp(this, auth);
        loginWithGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.login_with_google:
                        signIn();
                        break;
                }
            }
        });
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                presenter.firebaseAuthWithGoogle(account, this);
                Toast.makeText(this, "Authentication successful.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Authentication failed.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void goToSecondActivity() {
        Intent intent = new Intent(this, ChatActivity.class);
        startActivity(intent);
    }

    @Override
    public void signInResult(boolean response) {
        if(response) {
            Toast.makeText(this, "Authentication successful", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Authentication failed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void googleApiClientReady(GoogleApiClient googleApiClient) {
        this.googleApiClient = googleApiClient;
    }

    public void authenticateUser(View view) {
        switch (view.getId()) {
            case R.id.btnLogIn:
                if (etUserName.getText().toString().equals("") || etPassword.getText().toString().equals(""))
                    Toast.makeText(this, "Email and password required", Toast.LENGTH_SHORT).show();
                else
                    presenter.signIn(etUserName.getText().toString(), etPassword.getText().toString());
                break;
            case R.id.btnRegister:
                Intent registerIntent = new Intent(this, RegisterActivity.class);
                startActivity(registerIntent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        auth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (authStateListener != null)
            auth.removeAuthStateListener(authStateListener);
        presenter.removeView();
        presenter.removeAuthStateListener();
    }
}