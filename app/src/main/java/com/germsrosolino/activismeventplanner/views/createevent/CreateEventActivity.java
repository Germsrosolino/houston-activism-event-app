package com.germsrosolino.activismeventplanner.views.createevent;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.widget.EditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.germsrosolino.activismeventplanner.R;
import com.germsrosolino.activismeventplanner.models.Event;
import com.germsrosolino.activismeventplanner.presenters.CreateEventActivityPresenter;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Calendar;

public class CreateEventActivity extends AppCompatActivity implements CreateEventActivityContract.view, View.OnClickListener {

    private static final int PICK_FROM_GALLERY = 2;
    ImageView ivEventLogo;
    ImageView ivStartGallery;
    TextInputLayout inputEventName;
    TextInputLayout inputLocation;
    TextInputLayout date;
    EditText inputDate;
    TextInputLayout startTime;
    EditText inputStartTime;
    TextInputLayout inputEndDate;
    TextInputLayout endTime;
    EditText inputEndTime;
    TextInputLayout inputDescription;

    private CreateEventActivityPresenter presenter;
    private Event event;
    private Bitmap bitmap;

    private static final int DIALOG_CAL = 0;
    private static final int DIALOG_TIME = 1;
    private Calendar calendar;
    private int year, month, day;
    private String hour, min, value;
    private String am_pm;
    private StringBuilder minutes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);

        presenter = new CreateEventActivityPresenter();
        presenter.attachView(this);

        date = (TextInputLayout) findViewById(R.id.event_date);
        startTime = (TextInputLayout) findViewById(R.id.event_start_time);
        endTime = (TextInputLayout) findViewById(R.id.event_end_time);
        inputEventName = (TextInputLayout) findViewById(R.id.event_name);
        inputLocation = (TextInputLayout) findViewById(R.id.event_location);
        inputDescription = (TextInputLayout) findViewById(R.id.event_description);
        ivStartGallery = (ImageView) findViewById(R.id.event_add_photo);
        ivEventLogo = (ImageView) findViewById(R.id.event_logo);

        inputDate = date.getEditText();
        inputStartTime = startTime.getEditText();
        inputEndTime = endTime.getEditText();

//        inputDate.setInputType(InputType.TYPE_NULL);
//        inputStartTime.setInputType(InputType.TYPE_NULL);
//        inputEndTime.setInputType(InputType.TYPE_NULL);
//        calendar = Calendar.getInstance();
//        year = calendar.get(Calendar.YEAR);
//        month = calendar.get(Calendar.MONTH);
//        day = calendar.get(Calendar.DAY_OF_MONTH);
//        hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
//        min = String.valueOf(calendar.get(Calendar.MINUTE));
//        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        showDate(year, month + 1, day, "start");
//        showStartTime(hour, min, "start");

        event = new Event();

        ivStartGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (ActivityCompat.checkSelfPermission(CreateEventActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(CreateEventActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                    } else {
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
                } else {
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_FROM_GALLERY && resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();
                bitmap = BitmapFactory.decodeFile(picturePath);
                ivEventLogo.setImageBitmap(bitmap);
            }

        }
    }

    public void createEvent(View view) {
        boolean valid = true;
        if(inputEventName.getEditText().getText().toString().equals("")) {
            inputEventName.setError("Please name your event");
            valid = false;
        }
        if(inputLocation.getEditText().getText().toString().equals("")) {
            inputLocation.setError("Location required");
            valid = false;
        }
        if(inputDate.getText().toString().equals("")) {
            inputDate.setError("Please enter the date");
            valid = false;
        }
        if(inputStartTime.getText().toString().equals("")) {
            inputStartTime.setError("Please enter the start time");
            valid = false;
        }
        if(inputEndTime.getText().toString().equals("")) {
            inputEndDate.setError("Please enter the end time");
            valid = false;
        }
        if(inputDescription.getEditText().getText().toString().equals("")) {
            inputDescription.setError("Description is required");
            valid = false;
        }
        if (valid) {
            event = new Event();

            event.setHostId(FirebaseAuth.getInstance().getCurrentUser().getUid());
            event.setEventName(inputEventName.getEditText().getText().toString());
            event.setDate(inputDate.getText().toString());
            event.setStartTime(inputStartTime.getText().toString());
            event.setEndDate(inputEndTime.getText().toString());
            event.setDescription(inputDescription.getEditText().getText().toString());
        }
        presenter.createNewEvent(event, bitmap);
    }



    @Override
    public void eventSaved(Boolean saved) {
        if(saved) {
            Toast.makeText(this, "Event Successfully saved", Toast.LENGTH_SHORT).show();
            onBackPressed();
        } else {
            Toast.makeText(this, "Event edit failed", Toast.LENGTH_SHORT).show();
        }
    }

    private void showDate(int year, int month, int day, String val) {
        if (val == "start") {
            inputDate.setText(new StringBuilder().append(month).append("/").append(day).append("/").append(year));
        }
    }

    private void showStartTime(String hour, String minute, String val) {
        if(Integer.parseInt(hour) < 12) {
            am_pm = "AM";
        } else {
            am_pm = "PM";
            hour = String.valueOf(Integer.parseInt(hour) - 12);
        }
        if(minute.length() == 1) {
            minutes= new StringBuilder().append("0").append(minute);
            minute = String.valueOf(minutes);
        } else {
            minutes = new StringBuilder().append(minute);
            minute = String.valueOf(minutes);
        }
        if(val == "start") {
            inputStartTime.setText(new StringBuilder().append(hour).append(":").append(minute).append(" ").append(am_pm));
        } else {
            inputEndTime.setText(new StringBuilder().append(hour).append(":").append(minute).append(" ").append(am_pm));
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.event_date:
                value = "start";
                break;
            case R.id.event_start_time:
                value = "start";
                break;
            case R.id.event_end_time:
                value = "end";
                break;
        }
    }
}
