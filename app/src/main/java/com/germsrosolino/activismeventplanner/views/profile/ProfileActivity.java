package com.germsrosolino.activismeventplanner.views.profile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.germsrosolino.activismeventplanner.R;
import com.germsrosolino.activismeventplanner.presenters.ProfileActivityPresenter;
import com.germsrosolino.activismeventplanner.utils.FirebaseEventManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ProfileActivity extends AppCompatActivity implements ProfileActivityContract.view {

    ProfileActivityPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        setTitle("Profile");

        presenter.attachView(this);
        presenter.setContext(this);

    }


}
