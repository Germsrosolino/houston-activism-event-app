package com.germsrosolino.activismeventplanner.views;

public interface BasePresenter<V extends BaseView> {
    void attachView(V view);
    void removeView();

}