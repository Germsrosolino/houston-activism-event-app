package com.germsrosolino.activismeventplanner.views.fragment;

import android.content.Context;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.AnimatorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageButton;
import android.transition.Transition;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.germsrosolino.activismeventplanner.R;
import com.germsrosolino.activismeventplanner.models.Event;
import com.germsrosolino.activismeventplanner.models.EventList;
import com.germsrosolino.activismeventplanner.models.MyAttending;
import com.germsrosolino.activismeventplanner.utils.Constant;
import com.germsrosolino.activismeventplanner.utils.FirebaseEventManager;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Map;

import static android.content.ContentValues.TAG;


public class EventFragment extends Fragment implements ChildEventListener, View.OnClickListener {
    private static final String TAG = "EventFragment";
    private FirebaseEventManager firebaseEventManager = new FirebaseEventManager();

    AppCompatImageButton btnAttending;
    AppCompatImageButton btnShareEvent;
    AppCompatImageButton btnReject;

    ImageView ivLogo;
    TextView tvEventName;
    TextView tvAttending;
    TextView tvDistance;
    TextView tvDesc;

    private List<Event> events;
    private Event event;
    private Context context;


    public static EventFragment newInstance(String id) {
        Bundle args = new Bundle();
        args.putString(Constant.EVENT_ID, id);
        EventFragment fragment = new EventFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String id = getArguments().getString(Constant.EVENT_ID);
        EventList eventList = EventList.getInstance(getActivity());
        events = eventList.getEvents();
        event = getEvent(id);
    }

    public Event getEvent(String id) {
        for (int i = 0; i < events.size(); i++) {
            if (events.get(i).getId().equals(id)) {
                return events.get(i);
            }
        }
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.event_fragment_layout, container, false);
        btnAttending = view.findViewById(R.id.attending_button);
        btnReject = view.findViewById(R.id.reject_button);
        btnShareEvent = view.findViewById(R.id.share_button);
        tvDesc = view.findViewById(R.id.tvDesc);
        ivLogo = view.findViewById(R.id.event_pic);
        btnAttending.setOnClickListener(this);
        btnReject.setOnClickListener(this);
        btnShareEvent.setOnClickListener(this);
        tvEventName = view.findViewById(R.id.event_name);
        if (event != null) {
            Log.d(TAG, "onCreateView: " + event.getImageURL());
            tvEventName.setText(event.getEventName());
            tvDesc.setText(event.getDescription());

            loadEventImage(event.getImageURL(), ivLogo);

            if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                for (Map.Entry<String, String> entry : event.getIdsOfAttending().entrySet()) {
                    if (entry.getValue().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                        btnAttending.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_like_48dp));
                        break;
                    }
                }
            }

            float d = getResources().getDisplayMetrics().density;
//            if (event.getIdsOfAttending()!= null){
//                tvAttending.setText(String.valueOf(event.getIdsOfAttending().size()));
//            }else {
//                tvAttending.setText("0");
//            }
        }
        return view;
    }

    private void loadEventImage(String url, ImageView imageView) {
        if(event.getImageURL() != null) {
            Picasso.get().load(url).placeholder(R.drawable.alf).into(imageView);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.attending_button:
                if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                    if(!EventBus.getDefault().isRegistered(this)) {
                        EventBus.getDefault().register(this);
                        firebaseEventManager.getAttending();
                    }
                } else {
                    Toast.makeText(context, "You must sign in to attend", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.share_button:
                Toast.makeText(context, "This button is not yet working", Toast.LENGTH_SHORT).show();
                break;
            case R.id.reject_button:
                Toast.makeText(context, "This button is not yet working", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        final Event eventChanged = dataSnapshot.getValue(Event.class);
        eventChanged.setId(dataSnapshot.getKey());

        if(event != null) {
            if(eventChanged.getId().equals(event.getId())) {
                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageReference = storage.getReferenceFromUrl("gs://activism-planner.appspot.com");

                storageReference.child("images/" + eventChanged.getId() + ".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        tvEventName.setText(eventChanged.getEventName());
                        tvDesc.setText(eventChanged.getDescription());
                        loadEventImage(uri.toString(), ivLogo);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        tvEventName.setText(eventChanged.getEventName());
                        tvDesc.setText(eventChanged.getDescription());
                    }
                });
            }
        }
    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
        final Event eventRemoved = dataSnapshot.getValue(Event.class);
        eventRemoved.setId(dataSnapshot.getKey());

        if(event != null) {
            if (event.getId().equals(eventRemoved.getId())) {
                tvEventName.setText("This Event has been deleted");
                tvDesc.setText("");
                ivLogo.setImageBitmap(null);
            }
        }
    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MyAttending myAttending) {

        if(FirebaseAuth.getInstance().getCurrentUser() != null) {
            TransitionDrawable attend = (TransitionDrawable) ContextCompat.getDrawable(context, R.drawable.ic_like_48dp);
            TransitionDrawable unAttend = (TransitionDrawable) ContextCompat.getDrawable(context, R.drawable.ic_unlike);

            List<String> attending = myAttending.getAttending();

            if(!attending.contains(event.getId())) {
                btnAttending.setImageDrawable(attend);
                attend.reverseTransition(2000);
                btnAttending.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_unlike));

                firebaseEventManager
                        .removeAttending(FirebaseAuth.getInstance().getCurrentUser().getUid(),
                                event.getId(),
                                event.getHostId());
            }
            EventBus.getDefault().unregister(this);
        }
    }
}