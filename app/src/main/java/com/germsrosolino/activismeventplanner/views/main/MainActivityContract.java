package com.germsrosolino.activismeventplanner.views.main;

import com.germsrosolino.activismeventplanner.views.BasePresenter;
import com.germsrosolino.activismeventplanner.views.BaseView;

public interface MainActivityContract {

    interface MainView extends BaseView{
    }

    interface Presenter extends BasePresenter<MainView>{
        void getEventsFromFirebase();
    }
}
