package com.germsrosolino.activismeventplanner.interfaces;

import com.germsrosolino.activismeventplanner.models.Chat;

import java.util.ArrayList;

public interface ModelCallBacks {
    void onModelUpdated(ArrayList<Chat> messages);
}
