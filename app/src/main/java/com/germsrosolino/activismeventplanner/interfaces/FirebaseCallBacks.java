package com.germsrosolino.activismeventplanner.interfaces;

import com.google.firebase.database.DataSnapshot;

public interface FirebaseCallBacks {
    void onNewMessage (DataSnapshot dataSnapshot);
}
