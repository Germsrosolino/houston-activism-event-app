package com.germsrosolino.activismeventplanner.interfaces;

import android.content.Context;
import android.graphics.Bitmap;

import com.germsrosolino.activismeventplanner.models.Event;

public interface FirebaseInterface {
    void getAllEvents(Context context);
    void saveAttending(String userId, String eventId, String hostId);
    void sendFriendRequest(String currentUserId, String friendUserId);
    void acceptFriendRequest(String currentUserId, String friendUserId);
    void getMyFriends();
    void removeAttending(String userId, String eventId, String hostId);
    void removeFriend(String userId, String friendUserId);
    void getAttending();
    void getPendingFriendRequests();
    void addEvent(Event event, Bitmap bitmap);
    void editEvent(Event event, Bitmap bitmap);
    void updateImageURL(String url);
    void getMyUserName();
    void updateUserName(String userName, String oldUserName);
}
